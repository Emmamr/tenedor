# Getting started

`sudo npm install && npm start`

## Features

* Search for endless amounts of recipes! (But don't cause the API only allows 50 hits per day or else I have to pay and ain't no body got money like that!)

* Browse to the resipes and click on one you like to see the incredients

* Change the Servings amount

* Add the ingredients to your shopping list

* Edit the items in your shopping list

* Favorite a recipe you like and thanks to `localStorage` it will remember you favorite recipes.

## Things to maybe improve

* Implement button to delete all shopping list items

* Implement functionality to manually add items to shopping list;

* Save shopping list data in local storage; 

* Improve the ingredient parsing algorithm;

* Come up with an algorithm for calculating the amount of servings;

* Improve error handling